package jar;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.Callable;

public class Worker implements Runnable {
    private Thread self;
    private int id;
    private final Object workerNotifier;
    private final Object managerNotifier;
    private Manager manager;

    public void doJob() throws InterruptedException {
        synchronized (this.managerNotifier) {
            System.out.println("Worker " + this.id + ": Job Started");
            self.sleep(500);
            System.out.println("Worker " + this.id + ": Job Done");
        }
    }

    @Override
    public void run() {
        try {
                //TODO пишем код работы и синхронизации тут
                this.doJob();
                this.notifyJobDone();
            synchronized (workerNotifier) {
                workerNotifier.wait();
                this.doJob();
                this.notifyJobDone();
                /**Немного boilerplate, для красивой работы нужен управляющий компонент
                 * и чуть более красивый алгоритм запуска рабочих*/
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void subscribe(Manager manager) {
        this.manager = manager;
    }

    public void unsubscribe(Manager manager) {
        //TODO пока оставим как есть
    }

    public void notifyJobDone() {
        synchronized (managerNotifier) {
            this.manager.update(this.id);
            managerNotifier.notify();
        }
    }


    public void start() {
        self.start();
    }

    public void interrupt() {
        self.interrupt();
    }

    public void join() {
        try {
            self.join();
        } catch (InterruptedException ex) {
            System.out.println("Worker " + this.id + "\n" + ex.getStackTrace());
        }
    }

    public Worker(int id) {
        self = new Thread(this);
        this.id = id;
        this.workerNotifier = Manager.getInstance().getWorkerNotifier();
        this.managerNotifier = Manager.getInstance().getManagerNotifier();
    }

    public int getId() {
        return id;
    }

}
