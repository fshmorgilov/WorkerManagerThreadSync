package jar;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class UtilsAndConfigs {
    public static final Object WORKERNOTIFIER = new Object();
    public static final Object MANAGERNOTIFIER = new Object();
    public static final int WORKERCOUNT = 4;
    public static final Random RANDOM = new Random();
    public static ExecutorService executorService = Executors.newWorkStealingPool();

}
