package jar;


import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class App {

    public static void main(String[] args) {
        for (int i = 0; i < UtilsAndConfigs.WORKERCOUNT; i++) {
            Manager.getInstance().getJobDone().add(i, false);
        }
        //DEBUG
        Manager.getInstance().getJobDone().forEach(x ->
                System.out.println("DEBUG: " + x.toString())
        );
        //END DEBUG
        Manager.getInstance().start();

        List<Worker> workerList = IntStream.iterate(0, x -> x = x + 1)
                .limit(UtilsAndConfigs.WORKERCOUNT)
                .mapToObj(Worker::new)
                .peek(x -> x.subscribe(Manager.getInstance()))
                .peek(x -> System.out.println("Worker " + x.getId() + " created"))
                .peek(Worker::start)
                .collect(Collectors.toList());

//            workerList.forEach(Worker::join);
        //DEBUG
//        Manager.getInstance().getJobDone().forEach(x ->
//                System.out.println("DEBUG: " + x.toString())
//        );
        //END DEBUG

    }
}
