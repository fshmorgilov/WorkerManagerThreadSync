package jar;

import lombok.Getter;

import java.util.ArrayList;

public class Manager implements Runnable, WorkerSubscriber {
    private static volatile Manager self;
    private Thread selfThread;
    private final Object workerNotifier;
    private final Object managerNotifier;
    private volatile ArrayList<Boolean> jobDone = new ArrayList<>();
//    private boolean active = false;

    public static Manager getInstance() {
        {
            synchronized (App.class) {
                if (self == null) {
                    self = new Manager();
                    System.out.println("Manager created");
                }
            }
            return self;
        }
    }

    @Override
    public void update(int id) {
        this.jobDone.set(id, true);
    }

    public boolean allJobsAreDone() {
        return this.jobDone.stream().allMatch(x -> x == true);
    }


    public void report() {
        System.out.println("All jobs are completed\n ---------------------");
        this.jobDone.forEach(x -> x = false);
    }


    @Override
    public void run() {
        System.out.println("Manager started");
        try {
            synchronized (managerNotifier) {
                managerNotifier.wait();
            }
            if (allJobsAreDone()) {
                report();
                synchronized (workerNotifier) {
                    workerNotifier.notifyAll();
                    System.out.println("Manager stopped");
                }
                /**Нужен управляющий компонент, который по команде менеджера будет перезапускать всех рабочих*/

            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void start() {
        selfThread.start();
    }

    public void interrupt() {
        selfThread.interrupt();
    }

    private Manager() {
        this.selfThread = new Thread(this);
        this.managerNotifier = UtilsAndConfigs.MANAGERNOTIFIER;
        this.workerNotifier = UtilsAndConfigs.WORKERNOTIFIER;
    }

    public ArrayList<Boolean> getJobDone() {
        return jobDone;
    }

    public Object getWorkerNotifier() {
        return workerNotifier;
    }

    public Object getManagerNotifier() {
        return managerNotifier;
    }
}
