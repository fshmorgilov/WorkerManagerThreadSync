package jar;

public interface WorkerSubscriber {

    void update(int id);
}
